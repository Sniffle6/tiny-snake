
namespace game {

    /** New System */
	export class GameService {

		static Restart(world: ut.World) {
			game.FoodSpawnerSystem.MovePosition(world);
			TailSystem.RemoveTails(world);
			InputMovementSystem.ResetMovement(world);
			game.ScoreSystem.ResetScore(world);
		}
    }
}
