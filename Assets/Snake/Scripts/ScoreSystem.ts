
namespace game {

    /** New System */
    export class ScoreSystem extends ut.ComponentSystem {

		static scoreEntity: ut.Entity = null;
		OnUpdate(): void {
			if (game.ScoreSystem.scoreEntity == null) {
				this.world.forEach([ut.Entity, game.ScoreTag], (entity, tag) => {
					game.ScoreSystem.scoreEntity = new ut.Entity(entity.index, entity.version);
				});
			}
		}

		static OnGainScore(world: ut.World): void {
			world.usingComponentData(this.scoreEntity, [ut.Text.Text2DRenderer, game.Score], (text, score) => {
				score.score += 1; 
				text.text = score.score.toString();
			});
		}

		static ResetScore(world: ut.World): void {
			world.usingComponentData(this.scoreEntity, [ut.Text.Text2DRenderer, game.Score], (text, score) => {
				score.score = 0;
				text.text = score.score.toString();
			});
		}


    }
}
