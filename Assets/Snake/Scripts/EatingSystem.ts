
namespace game {

	/** New System */
	export class EatingSystem extends ut.ComponentSystem {

		OnUpdate(): void {
			let didHit = false;

			this.world.forEach([ut.Entity, ut.Core2D.TransformLocalPosition, ut.HitBox2D.HitBoxOverlapResults, game.FoodTag], (entity, position, contacts, tag) => {
				if (this.world.hasComponent(contacts.overlaps[0].otherEntity, game.PlayerTag)) {
					didHit = true;
					this.world.destroyEntity(entity);
					ScoreSystem.OnGainScore(this.world);
				}


			});
			if (didHit) {
				game.TailSystem.SpawnTail(this.world, game.TailSystem.tailObject);
			}
		}
	}
}
