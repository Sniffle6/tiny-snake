
namespace game {

	/** New System */
	@ut.executeAfter(ut.Shared.InputFence)
    export class InputMovementSystem extends ut.ComponentSystem {

		static counter: number = 0;
		static cooldown: number = 0.1;
		static player: ut.Entity = null;
		OnUpdate(): void {

			let moved = false;
			let dt = this.scheduler.deltaTime();

			if (game.InputMovementSystem.player == null) {
				this.world.forEach([ut.Entity, game.PlayerTag], (entity, tag) => {
					game.InputMovementSystem.player = new ut.Entity(entity.index, entity.version);
				});
			}

			game.InputMovementSystem.counter += dt;

			this.world.usingComponentData(game.InputMovementSystem.player, [ut.Core2D.TransformLocalPosition, game.MoveSpeed, game.Boundaries, game.Direction, game.LastPosition], (position, speed, bounds, direction, lastPosition) => {

				if (ut.Runtime.Input.getKey(ut.Core2D.KeyCode.W))
					direction.direction = new Vector3(0, speed.speed);
				if (ut.Runtime.Input.getKey(ut.Core2D.KeyCode.S))
					direction.direction = new Vector3(0, -speed.speed);
				if (ut.Runtime.Input.getKey(ut.Core2D.KeyCode.A))
					direction.direction = new Vector3(-speed.speed, 0);
				if (ut.Runtime.Input.getKey(ut.Core2D.KeyCode.D))
					direction.direction = new Vector3(speed.speed, 0);



				if (game.InputMovementSystem.counter > game.InputMovementSystem.cooldown) {
					game.InputMovementSystem.counter = 0;

					let localPosition = position.position;
					var lastPos = new Vector3(localPosition.x, localPosition.y, localPosition.z);

					localPosition.add(direction.direction);

					localPosition.x = Math.max(bounds.minX, Math.min(bounds.maxX, localPosition.x));
					localPosition.y = Math.max(bounds.minY, Math.min(bounds.maxY, localPosition.y));
					if (!localPosition.equals(lastPos)) {
						lastPosition.position = lastPos;
						position.position = localPosition;
						moved = true;
					}

				}
			});
			if (moved)
				game.TailSystem.OnMove(this.world);
		}


		static ResetMovement(world: ut.World): void {
			world.usingComponentData(this.player, [ut.Core2D.TransformLocalPosition, game.LastPosition, game.Direction], (position, lastPos, dir) => {
				let zero = new Vector3(0, 0, 0);
				lastPos.position = zero;
				position.position = zero;
				dir.direction = zero;
			});
		}

    }
}
