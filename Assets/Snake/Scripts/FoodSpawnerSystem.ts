
namespace game {    /** New System */
	export class FoodSpawnerSystem extends ut.ComponentSystem {
		static food: ut.Entity;
		static foodObject: string = "game.FoodGroup";

		OnUpdate(): void {
			if (!this.world.exists(game.FoodSpawnerSystem.food)){
				game.FoodSpawnerSystem.Spawn(this.world, game.FoodSpawnerSystem.foodObject);
			}
		}

		static Spawn(world: ut.World, entityGroup: string) {
			this.food = Utils.Spawn(world, entityGroup, new Vector3(Utils.GetRandom(-8, 8), Utils.GetRandom(-4, 4)));
		}

		static MovePosition(world: ut.World) {
			world.usingComponentData(this.food, [ut.Core2D.TransformLocalPosition], (position) => {
				position.position = new Vector3(Utils.GetRandom(-8, 8), Utils.GetRandom(-4, 4));
			});
		}

    }
}
