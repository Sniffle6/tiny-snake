
module Utils {

	export function Spawn(world: ut.World, entityGroup: string, position: Vector3 = null, entityIndex: number = 0) {
		let entity = ut.EntityGroup.instantiate(world, entityGroup)[entityIndex];

		if (position != null) {
			world.usingComponentData(entity, [ut.Core2D.TransformLocalPosition], (transformLocalPosition) => {
				transformLocalPosition.position = position;
			});
		}
		return new ut.Entity(entity.index, entity.version);
	}

	export function GetRandom(min, max) {
		return Math.random() * (max - min + 1) + min;
	}

}
