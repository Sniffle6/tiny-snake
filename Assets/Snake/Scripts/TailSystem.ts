
namespace game {

    /** New System */
    export class TailSystem extends ut.ComponentSystem {
		static tailObject = "game.TailGroup";
		static tailArray: Array<ut.Entity> = new Array<ut.Entity>();
        OnUpdate():void {
			let gameover = false;
			this.world.forEach([ut.HitBox2D.HitBoxOverlapResults, game.TailTag], (contacts, tag) => {
				if (this.world.hasComponent(contacts.overlaps[0].otherEntity, game.PlayerTag)) {
					gameover = true;
				}
			});
			if (gameover)
				game.GameService.Restart(this.world);

		}
		static OnMove(world: ut.World): void {

			if (this.tailArray.length == 0)
				return;
			var lastTail = this.tailArray.pop();
			world.usingComponentData(lastTail, [ut.Core2D.TransformLocalPosition], (transformLocalPosition) => {
				transformLocalPosition.position = world.getComponentData(InputMovementSystem.player, game.LastPosition).position;

			});
			this.tailArray.unshift(lastTail);
		}

		static SpawnTail(world: ut.World, entityGroup: string) {
			let tail = Utils.Spawn(world, entityGroup, world.getComponentData(InputMovementSystem.player, game.LastPosition).position);
			this.tailArray.push(new ut.Entity(tail.index, tail.version));
		}
		static RemoveTails(world: ut.World) {
			this.tailArray = [];
			ut.EntityGroup.destroyAll(world, this.tailObject);
		}

    }
}
